# Mengenal _Single Sign-On_
---
Apabila anda membuat website, maka akan sangat terbantu dengan adanya Disqus karena anda tidak perlu lagi memikirkan mengenai pembuatan fitur komentar, karena anda tinggal memasang script yang disediakan Disqus pada halaman website milik anda.