# Implementasi Disqus Single Sign-On \(SSO\) pada Django
---
Oleh : [Aditya Gusti Tammam](https://twitter.com/gustitammam) pada [Tutor Lokal](https://tutorlokal.blogspot.com)

Artikel ini membahas mengenai cara implementasi atau mengaktifkan fitur *Single Sign-On* (SSO) milik Disqus ke project yang menggunakan web framework Django. Berikut ini poin-poin yang akan dibahas nanti :
* Apa itu Disqus ?
* Mengenal *Single Sign-On* (SSO)
* Menggunakan module **django-disqus**
* Menampilkan komentar Disqus pada Django
* Implementasi *Single Sign-On* menggunakan **django-disqus**

