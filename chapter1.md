# Apa itu Disqus ?
---
![](/assets/disqus_logo.png)
Disqus merupakan platform komentar yang banyak digunakan oleh ribuan situs di internet. Dengan menggunakan Disqus, website kita bisa mendapatkan sistem komentar degan fitur yang kaya dan lengkap dengan integrasi dengan media sosial, administrasi dan moderasi tinngkat lanjut, dan juga fungsi forum. Lebih hebatnya lagi untuk memasang disqus di web tidaklah sulit, cukup memasang script yang disediakan oleh disqus pada area yang ingin kita terapkan fitur komentar dan disqus akan muncul pada halaman web tersebut.
Supaya bisa mendapatkan script untuk website anda, tentu saja anda perlu mendaftarkan diri ke [Disqus](https://disqus.com/) dan membuat forum baru untuk web anda.

